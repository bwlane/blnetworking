# BLNetworking

A small enum-favoring package for performing predictable network operations.

Based on the presentation of Glenna Buford [here](https://www.youtube.com/watch?v=D_4OMSn_YYs) .
