//
//  APIRouter.swift
//  
//
//  Created by Brian Lane on 12/26/19.
//

import Foundation
// This follows https://www.youtube.com/watch?v=D_4OMSn_YYs

/// Protocol that enforces an implementer to have URLRequest field
public protocol APIRouter {
    var urlRequest: URLRequest { get }
}
