//
//  NetworkOperation.swift
//
//
//  Created by Brian Lane on 12/26/19.
//

import Foundation

/// Encapsulates the behaviors of a network operation in all states of performing a request
open class NetworkOperation<T>: Operation, NetworkOperationProtocol {
    
    public let request: URLRequest
    open var completion: ((Result<T?, Error>) -> Void)?
    
    open var desiredResult: T?
    
    public init(with apiRouter: APIRouter, completion: ((Result<T?, Error>) -> Void)? = nil) {
        self.request = apiRouter.urlRequest
        self.completion = completion
        self.operationState = .none
    }
    
    override public func start() {
        if !isExecuting {
            operationState = .executing
            performRequest()
        }
    }
    
    open func performRequest() {
        preconditionFailure("Must be overridden by subclasses")
        //Should this just be main()?
    }
    
    //MARK: operation state overrides
    
    override public var isReady: Bool {
        return super.isReady && operationState == .ready
    }
    
    override public var isExecuting: Bool {
        return operationState == .executing
    }
    
    override public var isFinished: Bool {
        return operationState == .finished
    }
    
    override public var isAsynchronous: Bool {
        return true
    }
    
    // MARK: NetworkOperationProtocol
    public var operationState: OperationState = .none {
        willSet {
            willChangeValue(forKey: newValue.rawValue)
        }
        
        didSet {
            didChangeValue(forKey: operationState.rawValue)
        }
    }
    
}

/// Helper implementation of network operation anticipating a response of a decodable type
public class DecodableNetworkOperation<T: Decodable>: NetworkOperation<T> {
    
    public override init(with apiRouter: APIRouter, completion: ((Result<T?, Error>) -> Void)? = nil) {
        super.init(with: apiRouter, completion: completion)
    }
    
    override public func performRequest() {
        let task: URLSessionDataTask = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            guard let self = self else { return }
            if let error: Error = error {
                self.completion?(Result.failure(error))
            } else {
                guard let data: Data = data else {
                    self.completion?(Result.success(nil))
                    return
                }
                do {
                    let decoded: T = try JSONDecoder().decode(T.self, from: data)
                    self.completion?(Result.success(decoded))
                } catch {
                    self.completion?(Result.failure(error))
                }
            }
        }
        
        task.resume()
    }
}

public protocol NetworkOperationProtocol: AnyObject {
    var operationState: OperationState { get set }
}

public enum OperationState: String {
    case none
    case ready = "isReady"
    case executing = "isExecuting"
    case finished = "isFinished"
}
