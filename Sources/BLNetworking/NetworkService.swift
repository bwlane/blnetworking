//
//  NetworkService.swift
//  
//
//  Created by Brian Lane on 12/26/19.
//

import Foundation

/// Error types for network service ops
public enum NetworkError: Error {
    case operationFailed
    case error(Error)
}

/// Singleton service that can conduct network operations in a queue
public struct NetworkService {
    public init() {}
    let operationQueue: OperationQueue = OperationQueue()
    
    // This syntax means the parameter type is both the type on the left
    // and is satisfying the protocol on the right
    public func add(_ operation: Operation & NetworkOperationProtocol) {
        operation.operationState = .ready
        operationQueue.addOperation(operation)
    }
}

public extension NetworkService {
  static var shared: NetworkService {
    return NetworkService()
  }
}
