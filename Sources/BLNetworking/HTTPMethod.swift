import Foundation

/// sum type of HTTP methods that can be accessed for raw values
public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
