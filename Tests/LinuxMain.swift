import XCTest

import BLNetworkingTests

var tests = [XCTestCaseEntry]()
tests += BLNetworkingTests.allTests()
XCTMain(tests)
